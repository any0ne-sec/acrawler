"""
爬虫模块入口
"""
from library import engine
from datetime import datetime
from library import library as clib

connect = clib.Connect()

# 传入时间控制器
# real:2021.11.13-2021.11.19，fake:2021.11.12-2021.11.20
start_date = datetime(2021, 11, 12)
end_date = datetime(2021, 11, 20)
real_start_date = datetime(2021, 11, 13)
real_end_date = datetime(2021, 11, 19)

# 传入json文件存储路径
json_directory = connect.get_path() + '/txt/'

# 给定要爬取文章的平台
platforms = ["xianzhi","freebuf", "anquanke"]

# 获取json文件名
json_filename = connect.format_date(real_start_date) + '-' + connect.format_date(real_end_date) + '.txt'

# 创建并清空json文件
json_filepath = json_directory + json_filename
connect.empty_json_file(json_filepath)

if __name__ == "__main__":
    m = engine.more(platforms, start_date, json_filepath)
    m.run()

