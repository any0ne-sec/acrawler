"""
搜索引擎模板
"""
# -* - coding: UTF-8 -* -
from threading import Thread
from datetime import datetime
from library import library as clib

connect = clib.Connect()

"""
该类用于多线程启动爬虫
"""
class more:
    list = []
    start_date = datetime(2021, 11, 12)
    json_filename = ""
    threads = []
    res = {}
    num = 0

    def __init__(self, lists, start_date, json_filename, num=10):
        self.start_date = start_date
        self.json_filename = json_filename

        self.list = lists
        self.num = num
        for li in lists:
            self.res[li] = {}

    # 爬取功能
    def scrawler(self, scraping):
        package = __import__("crawler." + scraping, fromlist=[scraping])
        temp_class = getattr(package, scraping)
        temp = temp_class(self.start_date, self.num)

        temp.start()
        temp.save_file(self.json_filename)

    # 获取线程
    def getThread(self, scraping):
        return Thread(target=self.scrawler, args=[scraping])

    # 设置平台列表
    def setList(self, lists):
        self.list = lists

    # 设置文章爬取的截止日期
    def setStartDate(self, start_date):
        self.start_date = start_date

    # 运行方法
    def run(self):
        for li in self.list:
            self.threads.append(self.getThread(li))
        for t in self.threads:
            t.start()
        for t in self.threads:
            t.join()


