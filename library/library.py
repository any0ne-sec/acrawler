from selenium import webdriver
import time
import requests
import random
import os

chrome_options = webdriver.ChromeOptions()
chrome_options.add_argument('--headless')
chrome_options.add_argument('--disable-gpu')
prefs = {"profile.managed_default_content_settings.images":2}
chrome_options.add_experimental_option("prefs",prefs)
class Connect:
    oss = ""
    def __init__(self,oss=""):
        self.oss = oss
        chrome_options.add_argument('user-agent=' + self.getUA())
		
    def siteConnect(self,site,flag):
        res = ""
        cishu=0
        browser = webdriver.Chrome(chrome_options=chrome_options)
        browser.get(site)
        while res.find(flag) == -1:
            res = f"browser text = {browser.page_source}"
            cishu = cishu + 1
            time.sleep(0.1)
            if res.find(flag) != -1:
                return res
            if cishu > 1000:
                return '4ny0neSec_wrong'
        browser.quit()

    def siteConnect_with_no_flag(self,site):
        res = ""
        browser = webdriver.Chrome(chrome_options=chrome_options)
        browser.get(site)
        res = f"browser text = {browser.page_source}"
        if res != "":
            return res
        else:
            return '4ny0neSec_wrong'
        browser.quit()

    def request_with_no_flag(self,site):
        res = ""
        res = requests.get(site)
        if res != "":
            return res
        else:
            return '4ny0neSec_wrong'
        browser.quit()

    def getUA(self):
        ua = ""
        random.seed(time.time())
        syslist = ["Windows","Mac","Linux","Andriod"]
        headers = {
                    "Windows":["Mozilla/5.0 (Windows NT 10.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/99.0.7113.93 Safari/537.36","Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:90.0) Gecko/20100101 Firefox/90.0","Mozilla/5.0 (Windows NT 6.1; Trident/7.0; rv:11.0) like Gecko"], 
                    "Mac":["Mozilla/5.0 (Macintosh; Intel Mac OS X 11_3) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/14.1 Safari/605.1.15","Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/99.0.3538.77 Safari/537.36","Mozilla/5.0 (Macintosh; Intel Mac OS X 10.15; rv:89.0) Gecko/20100101 Firefox/89.0"],
                    "Linux":["Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:64.0) Gecko/20100101 Firefox/64.0","Mozilla/5.0 (X11; U; Linux i686; en-US) AppleWebKit/532.4 (KHTML, like Gecko) Chrome/4.0.237.0 Safari/532.4 Debian","Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.85 Safari/537.36 OPR/76.0.4017.94"],
                    "Andriod":["Mozilla/5.0 (Linux; Android 11; Pixel 5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.91 Mobile Safari/537.36","Mozilla/5.0 (Android 11; Mobile; LG-M255; rv:88.0) Gecko/88.0 Firefox/88.0","Mozilla/5.0 (Linux; Android 6.0.1; NEO-U9-H) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.105 Safari/537.36 OPR/63.3.3216.58675"]
                    
        }

        if self.oss == "":
            r = random.randint(0,len(headers.keys())-1)
            k = headers.get(syslist[random.randint(0,r)])
            ua = k[random.randint(0,len(k)-1)]
        else :
            try:
                k = headers.get(self.oss)
                ua = k[random.randint(0,len(k)-1)]
            except TypeError as e:
                print("Error OS.")
                exit(0)
        return ua

    # 从2021-11-12 00:00:00（datetime）到2021.11.12
    def format_date(self, date):
        date = str(date)
        format_date = date[:10].replace('-','.')
        return str(format_date)

    def empty_json_file(self, filename = ""):
        f = open(filename,'w+')
        f.write("")
        f.close()

    # 获取当前路径
    def get_path(self):
        return os.getcwd()